# Test randomdensity

\test randomdensity
\todo Describe what this test does
\todo The test does not run

    domain cannot be divided by meshes of given gridsize
    ERROR for processor           0 :
    domain cannot be divided by meshes of given gridsize

# Setup instructions

Setup this test case with

    $AMRVAC_DIR/setup.pl -d=22 -phi=0 -z=0 -g=14,14 -p=hd -eos=default -nf=0 -ndust=0 -u=nul -arch=default

# Description

This test has no description yet.


